from rest_framework import serializers
from .models import Referencias, Cursos, Facturacion, Alumnos, Pagos, Inscripciones, Padre_Encargado

class ReferenciasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Referencias
        fields = '__all__'

class CursosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cursos
        fields = '__all__'

class FacturacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Facturacion
        fields = '__all__'

class AlumnosSerializer(serializers.ModelSerializer):
    ID_Facturacion = FacturacionSerializer()

    class Meta:
        model = Alumnos
        fields = ['Cedula', 'Nombre', 'Apellido', 'Fecha_Nac', 'ID_Facturacion', 'ID_alumno']
        
        
class Padre_EncargadoSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Padre_Encargado
        fields = '__all__'

class PagosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pagos
        fields = '__all__'
        

class InscripcionesSerializer(serializers.ModelSerializer):
    alumno = AlumnosSerializer(source='ID_alumno')
   # curso = serializers.PrimaryKeyRelatedField(queryset=Cursos.objects.all(), source='ID_Curso')

    class Meta:
        model = Inscripciones
        fields = ['alumno', 'ID_Curso', 'Fecha_inscripcion', 'Firmado', 'Estado']
        

    def create(self, validated_data):
       alumno_data = validated_data.pop('ID_alumno')
       facturacion_data = alumno_data.pop('ID_Facturacion')
       
       # Crear la instancia de Facturacion
       facturacion = Facturacion.objects.create(**facturacion_data)
       
       # Crear la instancia de Alumno asociada con la facturacion
       alumno = Alumnos.objects.create(ID_Facturacion=facturacion, **alumno_data)
        
       
       # Asociar el alumno creado a la inscripción
       inscripcion = Inscripciones.objects.create(ID_alumno=alumno, **validated_data)

       
       
       return inscripcion
            


