from rest_framework import routers
from .api import *

router = routers.DefaultRouter()

router.register('api/inscripciones', InscripcionesViewSet, 'inscripciones')
router.register('api/cursos', CursosViewSets, 'cursos')
router.register('api/alumnos', AlumnosViewSets, 'alumnos')
router.register('api/padres', PadresViewSets, 'padres')
router.register('api/pagos', PagosViewSets, 'pagos')
router.register('api/referencias', ReferenciasViewSets, 'referencias')

urlpatterns = router.urls