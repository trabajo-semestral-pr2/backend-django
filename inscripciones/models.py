from django.db import models

# Create your models here.
class Referencias(models.Model):
    ID_Referencia = models.AutoField(primary_key=True)  # Clave primaria auto-incrementable
    Abreviatura = models.CharField(max_length=50, unique=True)  # No puede haber abreviaturas duplicadas
    Cod_Ref = models.CharField(max_length=50)
    Descripcion = models.CharField(max_length=100)

class Cursos(models.Model):
    ID_Curso = models.AutoField(primary_key=True)  # Clave primaria auto-incrementable
    Descripcion = models.CharField(max_length=100)
    Nivel = models.CharField(max_length=50)
    Ciclo = models.CharField(max_length=50)
    Turno = models.CharField(max_length=50)
    Especializacion = models.CharField(max_length=50, blank=True)  # Campo opcional
    Matricula = models.IntegerField()
    Cuota = models.IntegerField()

class Facturacion(models.Model):
    ID_Facturacion = models.AutoField(primary_key=True)  # Clave primaria auto-incrementable
    Ruc = models.CharField(max_length=50) 
    Razon_Social = models.CharField(max_length=150)

class Alumnos(models.Model):
    ID_alumno = models.AutoField(primary_key=True)  # Clave primaria auto-incrementable
    Cedula = models.CharField(max_length=10, unique=True)  # No puede haber cédulas duplicadas
    Nombre = models.CharField(max_length=100)
    Apellido = models.CharField(max_length=100)
    Fecha_Nac = models.DateField()
    ID_Facturacion = models.ForeignKey(Facturacion, on_delete=models.CASCADE)  # Relacion con Facturacion

class Pagos(models.Model):
    ID_pago = models.AutoField(primary_key=True)  # Clave primaria auto-incrementable
    Monto = models.IntegerField()
    Fecha_pago = models.DateField()
    ID_Alumno = models.ForeignKey(Alumnos, on_delete=models.CASCADE)  # Relacion con Alumnos
    Tipo_Pago = models.CharField(max_length=50)

class Inscripciones(models.Model):
    ID_inscripcion = models.AutoField(primary_key=True)  # Clave primaria auto-incrementable
    ID_alumno = models.ForeignKey(Alumnos, on_delete=models.CASCADE)  # Relacion con Alumnos
    Fecha_inscripcion = models.DateField()
    Contrato = models.BinaryField(blank=True)  # Campo opcional para almacenar datos del contrato (archivos)
    Firmado = models.CharField(max_length=1)  # 'S' o 'N' para indicar si está firmado
    ID_Curso = models.ForeignKey(Cursos, on_delete=models.CASCADE)  # Relacion con Cursos
    Estado = models.CharField(max_length=50)

    


class Padre_Encargado(models.Model):
    ID_padre_encargado = models.AutoField(primary_key=True)  # Clave primaria auto-incrementable
    Cedula = models.CharField(max_length=10, unique=True)  # No puede haber cédulas duplicadas
    Nombre = models.CharField(max_length=100)
    Apellido = models.CharField(max_length=100)
    Direccion = models.CharField(max_length=260)
    Telefono = models.CharField(max_length=20)
    Correo = models.EmailField()  # Campo para correo electrónico con validación de formato
    ID_alumno = models.ForeignKey(Alumnos, on_delete=models.CASCADE)  # Relacion con Alumnos
    Emergencia = models.CharField(max_length=1)  # 'S' o 'N' para indicar si es contacto de emergencia
    Retiro = models.CharField(max_length=1)  # 'S' o 'N' para indicar si está retirado


