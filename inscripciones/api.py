from .models import Referencias, Cursos, Pagos, Inscripciones, Alumnos
from rest_framework import viewsets, permissions
from .serializers import *

class InscripcionesViewSet(viewsets.ModelViewSet):
    queryset = Inscripciones.objects.all()
    permissions_classes = [permissions.AllowAny]
    serializer_class = InscripcionesSerializer

class CursosViewSets(viewsets.ModelViewSet):
    queryset = Cursos.objects.all()
    permissions_classes = [permissions.AllowAny]
    serializer_class = CursosSerializer

class ReferenciasViewSets(viewsets.ModelViewSet):
    queryset = Referencias.objects.all()
    permissions_classes = [permissions.AllowAny]
    serializer_class = ReferenciasSerializer

class PagosViewSets(viewsets.ModelViewSet):
    queryset = Pagos.objects.all()
    permissions_classes = [permissions.AllowAny]
    serializer_class = PagosSerializer

class AlumnosViewSets(viewsets.ModelViewSet):
    queryset = Alumnos.objects.all()
    permissions_classes = [permissions.AllowAny]
    serializer_class = AlumnosSerializer

class PadresViewSets(viewsets.ModelViewSet):
    queryset = Padre_Encargado.objects.all()
    permissions_classes = [permissions.AllowAny]
    serializer_class = Padre_EncargadoSerializer